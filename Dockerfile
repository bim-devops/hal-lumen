FROM composer As composer-install 
COPY src/ /var/www/html/
WORKDIR /var/www/html/
RUN composer global require "laravel/lumen-installer"
RUN composer install

FROM php:7.2-apache
COPY 000-default.conf /etc/apache2/sites-available/
COPY --from=composer-install /var/www/html /var/www/html/
WORKDIR /var/www/html/
RUN apt-get update && apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip
RUN a2enmod rewrite

EXPOSE 80

